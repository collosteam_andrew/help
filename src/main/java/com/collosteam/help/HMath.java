package com.collosteam.help;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by collos on 03.12.15.
 */
public class HMath {
    private static int HOUR_MULTIPLIER = 3600;
    private static double[] UNIT_MULTIPLIERS = new double[]{0.001, 0.000621371192};
    public static int INDEX_KM = 0;
    public static int INDEX_MILES = 1;

    public static double roundDecimal(double value, final int decimalPlace) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(decimalPlace, RoundingMode.HALF_UP);
        value = bd.doubleValue();
        return value;
    }

    public static double convertSpeed(double speed, int measurement_index) {
        return ((speed * HOUR_MULTIPLIER) * UNIT_MULTIPLIERS[measurement_index]);
    }

}
