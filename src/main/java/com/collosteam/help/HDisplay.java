package com.collosteam.help;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by collos on 03.12.15.
 */
public class HDisplay {

    public static Point getScreenSize(Context mContext) {
        Point size = new Point();
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        android.view.Display display = wm.getDefaultDisplay();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB_MR1) {
            display.getSize(size);
        } else {
            size.set(display.getWidth(), display.getHeight());
        }
        return size;
    }

    public static int getScrOrientation(Context context) {
        Display getOrient = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = Configuration.ORIENTATION_PORTRAIT;
        if (getOrient.getWidth() == getOrient.getHeight()) {
            orientation = Configuration.ORIENTATION_SQUARE;
        } else { //if widht is less than height than it is portrait
            if (getOrient.getWidth() < getOrient.getHeight()) {
                orientation = Configuration.ORIENTATION_PORTRAIT;
            } else { // if it is not any of the above it will defineitly be landscape
                orientation = Configuration.ORIENTATION_LANDSCAPE;
            }
        }
        return orientation; // return value 1 is portrait and 2 is Landscape Mode
    }

}
