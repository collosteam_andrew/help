package com.collosteam.help;

import android.os.Build;

import java.util.UUID;

/**
 * Created by collos on 07.02.16.
 */
public class Help {

    /**
     * Hide default constructor.
     */
    private Help() {
        throw new UnsupportedOperationException();
    }

    /**
     * Get information if Android version is Kitkat (4.4).
     *
     * @return true if Kitkat.
     */
    public static boolean isKitkat() {
        return Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT;
    }

    /**
     * Get information if Android version is Lollipop (5.0) or higher.
     *
     * @return true if Lollipop or higher.
     */
    public static boolean isAndroid5() {
        return isAtLeastVersion(Build.VERSION_CODES.LOLLIPOP);
    }

    /**
     * Check if Android version is at least the given version.
     *
     * @param version The version
     * @return true if Android version is at least the given version
     */
    public static boolean isAtLeastVersion(final int version) {
        return Build.VERSION.SDK_INT >= version;
    }

    private static String fakeId;

    /**
     * Return pseudo unique ID
     *
     * @return ID
     */
    public static String getUniqueFakeID() {

        if (fakeId == null) {

            // If all else fails, if the user does have lower than API 9 (lower
            // than Gingerbread), has reset their device or 'Secure.ANDROID_ID'
            // returns 'null', then simply the ID returned will be solely based
            // off their Android device information. This is where the collisions
            // can happen.
            // Thanks http://www.pocketmagic.net/?p=1662!
            // Try not to use DISPLAY, HOST or ID - these items could change.
            // If there are collisions, there will be overlapping data
            String m_szDevIDShort = "35" + (Build.BOARD.length() % 10) + (Build.BRAND.length() % 10) + (Build.CPU_ABI.length() % 10) + (Build.DEVICE.length() % 10) + (Build.MANUFACTURER.length() % 10) + (Build.MODEL.length() % 10) + (Build.PRODUCT.length() % 10);

            // Thanks to @Roman SL!
            // http://stackoverflow.com/a/4789483/950427
            // Only devices with API >= 9 have android.os.Build.SERIAL
            // http://developer.android.com/reference/android/os/Build.html#SERIAL
            // If a user upgrades software or roots their device, there will be a duplicate entry
            String serial = null;
            try {
                serial = android.os.Build.class.getField("SERIAL").get(null).toString();

                // Go ahead and return the serial for api => 9
                return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
            } catch (Exception exception) {
                // String needs to be initialized
                serial = "serial"; // some value
            }

            // Thanks @Joe!
            // http://stackoverflow.com/a/2853253/950427
            // Finally, combine the values we have found by using the UUID class to create a unique identifier
            fakeId = new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        }
        return fakeId;
    }

}
