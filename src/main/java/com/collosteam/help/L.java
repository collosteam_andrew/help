package com.collosteam.help;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Arrays;


/**
 * Created by Collos on 12/28/2014.
 */
public class L {
    public static final String TAG = "L_";

    static boolean debug = true;
    @Nullable
    private static CustomLogger logger;

    public static void init(boolean debug, CustomLogger logger) {
        L.debug = debug;
        L.logger = logger;
    }

    public static void d(String tag, Object... msgs) {
        if (debug) {
            Log.d(tag, Arrays.toString(msgs));
        } else {
            if (logger != null) {
                logger.log(1001, tag, Arrays.toString(msgs));
            }
        }
    }

    public static void e(String tag, Object... msgs) {
        if (debug) {
            Log.e(tag, Arrays.toString(msgs));
        } else {
            if (logger != null) {
                logger.log(1002, tag, Arrays.toString(msgs));
            }
        }
    }

    //
    public static void exc(Throwable t) {
        if (debug) {
            Log.e(TAG, "LOG ERROR!", t);
        } else {
            if (logger != null) {
                logger.exc(t);
            }
        }
    }

    public interface CustomLogger {
        void log(int level, String tag, String msg);

        void exc(Throwable t);
    }
}
