package com.collosteam.help.ads;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.view.View;

import com.collosteam.help.L;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;

/**
 * Created by Collos on 9/28/2014.
 * <p>
 * Base ADS helper util class, use it for simple show banners
 */
public class AdMob {

    private static boolean inited = false;

    public static void setLogger(EventLogger logger) {
        AdMob.logger = logger;
    }

    public static void init(Context context, String publisherId) {
        inited = true;
        MobileAds.initialize(context, publisherId);
    }

    private static EventLogger logger;

    public interface EventLogger {
        void sendAnswerWithId(String tag, String action, String key);
    }

    private static final String TAG = "ADSHelper_";

    public static void update(@NonNull Activity activity, @IdRes int adViewResId, boolean ads) {
        update(activity, adViewResId, ads, true);
    }

    public static void update(@NonNull Activity activity, @IdRes int adViewResId, boolean ads, boolean autoShow) {
        if (activity.getWindow().getDecorView() != null) {
            update(activity.getWindow().getDecorView(), adViewResId, ads, autoShow);
        }
    }

    public static void update(@NonNull android.support.v4.app.Fragment fragment, @IdRes int adViewResId, boolean ads) {
        if (fragment.getView() != null) {
            update(fragment.getView(), adViewResId, ads, true);
        }
    }

    public static void update(@NonNull Fragment fragment, @IdRes int adViewResId, boolean ads) {
        if (fragment.getView() != null) {
            update(fragment.getView(), adViewResId, ads, true);
        }
    }

    //This method show simple ads test
    public static void update(@NonNull View rootView, @IdRes int adViewResId, boolean ads) {
        update(rootView, adViewResId, ads, true);
    }

    public static void update(@NonNull View rootView, @IdRes int adViewResId, boolean ads, final boolean autoShow) {
        if (!inited) {
            throw new IllegalStateException("Please init AdMob util!");
        }
        // The "loadAdOnCreate" and "testDevices" XML attributes no longer available.
        final View adView = rootView.findViewById(adViewResId);
        if (adView instanceof AdView) {
            handleAdView(ads, autoShow, (AdView) adView);
        } else {
            handleNativeAdView(ads, autoShow, (NativeExpressAdView) adView);
        }
    }

    private static void handleNativeAdView(boolean ads, final boolean autoShow, final NativeExpressAdView adView) {
        if (adView != null) {
            if (ads) {
                if (autoShow) {
                    adView.setVisibility(View.VISIBLE);
                }
                // Set the AdListener.
                adView.setAdListener(getAdListener(autoShow, adView, adView.getAdUnitId()));

                AdRequest adRequest = getAdRequest();
                adView.loadAd(adRequest);
            } else {
                adView.setVisibility(View.GONE);
                adView.destroy();
            }
        }
    }

    private static void handleAdView(boolean ads, final boolean autoShow, final AdView adView) {
        if (adView != null) {
            if (ads) {
                if (autoShow) {
                    adView.setVisibility(View.VISIBLE);
                }
                // Set the AdListener.
                adView.setAdListener(getAdListener(autoShow, adView, adView.getAdUnitId()));

                AdRequest adRequest = getAdRequest();
                adView.loadAd(adRequest);
            } else {
                adView.setVisibility(View.GONE);
                adView.destroy();
            }
        }
    }

    @NonNull
    private static AdListener getAdListener(final boolean autoShow, final View adView, final String adUnitId) {
        return new AdListener() {
            /**
             * Called when an ad is clicked and about to return to the application.
             */
            @Override
            public void onAdClosed() {
                L.d(TAG, "onAdClosed");
            }

            /**
             * Called when an ad failed to load.
             */
            @Override
            public void onAdFailedToLoad(int error) {
                String message = "onAdFailedToLoad: " + getErrorReason(error);
                L.d(TAG, message);
                adView.setVisibility(View.GONE);
            }

            /**
             * Called when an ad is clicked and going to start a new Activity that will
             * leave the application (e.g. breaking out to the Browser or Maps
             * application).
             */
            @Override
            public void onAdLeftApplication() {
                L.d(TAG, "onAdLeftApplication");
                if (adView != null && logger != null) {
                    logger.sendAnswerWithId(TAG, "Clicked", adUnitId);
                }
            }

            /**
             * Called when an Activity is created in front of the app (e.g. an
             * interstitial is shown, or an ad is clicked and launches a new Activity).
             */
            @Override
            public void onAdOpened() {
                L.d(TAG, "onAdOpened");
            }

            /**
             * Called when an ad is loaded.
             */
            @Override
            public void onAdLoaded() {
                L.d(TAG, "onAdLoaded");

                adView.setVisibility(View.VISIBLE);

                if (adView != null && logger != null) {
                    logger.sendAnswerWithId(TAG, "Showed", adUnitId);
                }
            }
        };
    }

    @NonNull
    private static AdRequest getAdRequest() {
        return new AdRequest.Builder()
                .addTestDevice("4AF602D59E886A853BD73B6C9A383FB3")
                .addTestDevice("DBD7395625AA1DBCD748E4C341AEC743")
                .addTestDevice("E386D68F1632503F93BD806D1DF36C96") //Nexus 4
                .addTestDevice("43E5AAB560A697635089BEA3F925CE35")
                .build();
    }

    /**
     * Gets a string error reason from an error code.
     */
    private static String getErrorReason(int errorCode) {
        String errorReason = "";
        switch (errorCode) {
            case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                errorReason = "Internal error";
                break;
            case AdRequest.ERROR_CODE_INVALID_REQUEST:
                errorReason = "Invalid request";
                break;
            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                errorReason = "Network Error";
                break;
            case AdRequest.ERROR_CODE_NO_FILL:
                errorReason = "No fill";
                break;
        }
        return errorReason;
    }
}
