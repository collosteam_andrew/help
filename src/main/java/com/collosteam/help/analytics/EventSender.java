package com.collosteam.help.analytics;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.XmlRes;
import android.text.TextUtils;

import com.collosteam.help.L;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by collos on 16.01.16.
 * Need implement this class like a static utils for send all events
 */
public class EventSender {

    private static final String TAG = "EventSender_";
    private static boolean debug;
    private static boolean init;

    private EventSender() {
    }

    private static Tracker tracker;
    private static Object object = new Object();

    /*Init default analytics system system on this simple method*/
    public static void init(Context context, @XmlRes int configResId, boolean debug) {
        EventSender.debug = debug;
        synchronized (object) {
            if (tracker == null) {
                tracker = GoogleAnalytics.getInstance(context).newTracker(configResId);
                tracker.enableAdvertisingIdCollection(true);
                init = true;
            }
        }
    }

    /**
     * Send simple google event if is no debug mode
     *
     * @param category name of group events
     * @param action   concrete event name
     * @param label    marker
     */
    public static void send(String category, String action, String label) {
        assert init;
        L.d(TAG, "Send event : category : " + category, "action : " + action, "label : " + label);
        if (!debug) {
            HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder().setCategory(category).setAction(action);
            if (!TextUtils.isEmpty(label)) {
                builder.setLabel(label);
            }
            tracker.send(builder.build());
        }
    }

    public static void send(String category, String action) {
        send(category, action, null);
    }


    public static synchronized void startActivity(@NonNull Activity activity) {
        assert init;
        L.d(TAG, "Start activity : " + activity);
        if (!debug) {
            GoogleAnalytics.getInstance(activity).reportActivityStart(activity);
        }
    }

    public static synchronized void stopActivity(@NonNull Activity activity) {
        assert init;
        L.d(TAG, "Stop activity : " + activity);
        if (!debug) {
            GoogleAnalytics.getInstance(activity).reportActivityStop(activity);
        }
    }

    public static void screen(@NonNull String screenName) {
        assert init;
        L.d(TAG, "Show screen : " + screenName);
        if (!debug) {
            tracker.setScreenName(screenName);
            tracker.send(new HitBuilders.AppViewBuilder().build());
        }
    }

    public static void duration(String category, String variable, String label, long startTime, long finishTime) {
        assert init;
        L.d(TAG, "Send time : category : " + category,
                " variable : " + variable,
                " label : " + label,
                " start time :" + startTime,
                " delta : " + (finishTime - startTime));
        if (!debug) {
            HitBuilders.TimingBuilder timingBuilder = new HitBuilders.TimingBuilder();
            timingBuilder.setCategory(category).setValue(finishTime - startTime).setVariable(variable);
            if (label != null) {
                timingBuilder.setLabel(label);
            }
            tracker.send(timingBuilder.build());
        }
    }

    public static void duration(String category, String variable, long startTime, long finishTime) {
        duration(category, variable, null, startTime, finishTime);
    }
}
