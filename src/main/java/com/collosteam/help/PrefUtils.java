package com.collosteam.help;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.StringRes;
import android.text.TextUtils;

import java.util.Map;
import java.util.Set;

/**
 * Created by collos on 23.04.15.
 */
public class PrefUtils {

    public static final String TAG = "PrefUtils_";

    private static SharedPreferences get(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static SharedPreferences get(Context context, String category) {
        return context.getSharedPreferences(category, Context.MODE_MULTI_PROCESS);
    }

    public static void saveLong(Context context, @StringRes int key, long data) {
        saveLong(context, context.getString(key), data);
    }

    public static void saveLong(Context context, String key, long data) {
        saveLong(context, null, key, data);
    }

    public static void saveLong(Context context, String category, String key, long data) {
        SharedPreferences.Editor editor = getEditor(context, category);
        editor.putLong(key, data);
        editor.apply();
    }

    public static void saveInt(Context context, String key, int data) {
        saveInt(context, null, key, data);
    }

    public static void saveInt(Context context, String category, String key, int data) {
        SharedPreferences.Editor editor = getEditor(context, category);
        editor.putInt(key, data);
        editor.apply();
    }

    private static SharedPreferences.Editor getEditor(Context context, String category) {
        SharedPreferences sp = category == null ? get(context) : get(context, category);
        return sp.edit();
    }

    public static void saveUri(Context context, String category, String key, Uri uri) {
        saveString(context, category, key, uri == null ? null : uri.toString());
    }

    public static void saveUri(Context context, String key, Uri uri) {
        saveString(context, key, uri == null ? null : uri.toString());
    }

    public static Uri getUri(Context context, String key, Uri defaultValue) {
        String tmp = getString(context, key, defaultValue == null ? null : defaultValue.toString());
        if (!TextUtils.isEmpty(tmp)) {
            try {
                return Uri.parse(tmp);
            } catch (Throwable e) {
                L.e(TAG, "Parse Uri ", e);
            }
        }
        return null;
    }

    public static Uri getUri(Context context, String category, String key, Uri defaultValue) {
        String tmp = getString(context, category, key, defaultValue == null ? null : defaultValue.toString());
        if (!TextUtils.isEmpty(tmp)) {
            try {
                return Uri.parse(tmp);
            } catch (Throwable e) {
                L.e(TAG, "Parse Uri ", e);
            }
        }
        return null;
    }


    public static void saveString(Context context, String key, String data) {
        saveString(context, null, key, data);
    }

    public static void saveString(Context context, String category, String key, String data) {
        SharedPreferences.Editor editor = getEditor(context, category);
        editor.putString(key, data);
        editor.apply();
    }

    public static void saveBoolean(Context context, String category, String key, boolean data) {
        SharedPreferences.Editor editor = getEditor(context, category);
        editor.putBoolean(key, data);
        editor.apply();
    }

    public static void saveBoolean(Context context, @StringRes int key, boolean data) {
        saveBoolean(context, null, context.getString(key), data);
    }

    public static void saveBoolean(Context context, String key, boolean data) {
        saveBoolean(context, null, key, data);
    }

    public static boolean getBoolean(Context context, @StringRes int key, boolean defaultValue) {
        return getBoolean(context, context.getString(key), defaultValue);
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        return get(context).getBoolean(key, defaultValue);
    }

    public static boolean getBoolean(Context context, String category, String key, boolean defaultValue) {
        return get(context, category).getBoolean(key, defaultValue);
    }

    public static int getInt(Context context, String key, int defaultValue) {
        return get(context).getInt(key, defaultValue);
    }

    public static int getInt(Context context, String category, String key, int defaultValue) {
        return get(context, category).getInt(key, defaultValue);
    }

    public static String getString(Context context, String key, String defaultValue) {
        return get(context).getString(key, defaultValue);
    }

    public static String getString(Context context, String category, String key, String defaultValue) {
        return get(context, category).getString(key, defaultValue);
    }

    public static boolean contains(Context context, String key) {
        return get(context).contains(key);
    }

    public static long getLong(Context context, @StringRes int key, long defaultValue) {
        return getLong(context, context.getString(key), defaultValue);
    }

    public static long getLong(Context context, String key, long defaultValue) {
        return get(context).getLong(key, defaultValue);
    }

    public static long getLong(Context context, String category, String key, long defaultValue) {
        return get(context, category).getLong(key, defaultValue);
    }

    public static void dump(Context context) {
        Map<String, ?> keys = get(context).getAll();
        L.d(TAG, "--------DUMP-------");
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            L.d(TAG, entry.getKey() + ": " +
                    entry.getValue().toString());
        }
        L.d(TAG, "-------------------");
    }

    public static Set<String> getStringSet(Context c, String key, Set<String> strings) {
        return getStringSet(c, null, key, strings);
    }

    public static Set<String> getStringSet(Context c, String category, String key, Set<String> strings) {
        return get(c, category).getStringSet(key, strings);
    }

    public static void saveStringSet(Context context, String key, Set<String> strings) {
        saveStringSet(context, null, key, strings);
    }

    public static void saveStringSet(Context context, String category, String key, Set<String> strings) {
        SharedPreferences.Editor editor = getEditor(context, category);
        editor.putStringSet(key, strings);
        editor.apply();
    }
}
