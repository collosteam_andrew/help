package com.collosteam.help;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.collosteam.help.HDialog.DisplayMessageDialogFragment.MessageDialogListener;

import java.io.Serializable;

/**
 * Created by collos on 07.02.16.
 */
public class HDialog {

    /**
     * Parameter to pass the title to the DialogFragment.
     */
    private static final String PARAM_TITLE = "title";
    /**
     * Parameter to pass the message to the DialogFragment (of all types).
     */
    private static final String PARAM_MESSAGE = "message";
    /**
     * Parameter to pass the text resource for the confirmation button to the ConfirmDialogFragment.
     */
    private static final String PARAM_BUTTON_RESOURCE = "buttonResource";
    /**
     * Parameter to pass the callback listener to the ConfirmDialogFragment.
     */
    private static final String PARAM_LISTENER = "listener";
    /**
     * Parameter to pass the key for the shared preference indicating if the tip should be shown.
     */
    private static final String PARAM_PREFERENCE_KEY = "keyPrefTip";
    private static final String TAG = "HDialog_";


    /**
     * Display an information message and go back to the current activity.
     *
     * @param activity the current activity
     * @param listener an optional listener waiting for the dialog response. If a listener is given, then the dialog will not
     *                 be automatically recreated on orientation change!
     * @param resource the message resource
     * @param args     arguments for the error message
     */
    public static void displayInfo(@NonNull final Activity activity, @Nullable final MessageDialogListener listener, final int resource,
                                   final Object... args) {
        String message = String.format(activity.getString(resource), args);
        Bundle bundle = new Bundle();
        bundle.putCharSequence(PARAM_MESSAGE, message);
        bundle.putString(PARAM_TITLE, activity.getString(R.string.title_dialog_info));
        if (listener != null) {
            bundle.putSerializable(PARAM_LISTENER, listener);
        }
        DialogFragment fragment = new DisplayMessageDialogFragment();
        fragment.setArguments(bundle);
        fragment.show(activity.getFragmentManager(), fragment.getClass().toString());
    }

    /**
     * Display an error and either go back to the current activity or finish the current activity.
     *
     * @param activity the current activity
     * @param resource the error message
     * @param listener listener to react on dialog confirmation or dismissal.
     * @param args     arguments for the error message
     */
    private static void displayError(@NonNull final Activity activity, final int resource, @Nullable final MessageDialogListener listener,
                                     final Object... args) {
        String message = String.format(activity.getString(resource), args);
        Log.w(TAG, "Dialog message: " + message);
        Bundle bundle = new Bundle();
        bundle.putCharSequence(PARAM_MESSAGE, message);
        bundle.putString(PARAM_TITLE, activity.getString(R.string.title_dialog_error));
        if (listener != null) {
            bundle.putSerializable(PARAM_LISTENER, listener);
        }

        DialogFragment fragment = new DisplayMessageDialogFragment();
        fragment.setArguments(bundle);
        fragment.show(activity.getFragmentManager(), fragment.getClass().toString());
    }

    /**
     * Display an error and either go back to the current activity or finish the current activity.
     *
     * @param activity       the current activity
     * @param resource       the error message
     * @param finishActivity a flag indicating if the activity should be finished.
     * @param args           arguments for the error message
     */
    public static void displayError(@NonNull final Activity activity, final int resource, final boolean finishActivity,
                                    final Object... args) {
        MessageDialogListener listener = null;

        if (finishActivity) {
            listener = new MessageDialogListener() {
                /**
                 * The serial version id.
                 */
                private static final long serialVersionUID = 1L;

                @Override
                public void onDialogClick(final DialogFragment dialog) {
                    activity.finish();
                }

                @Override
                public void onDialogCancel(final DialogFragment dialog) {
                    activity.finish();
                }
            };
        }
        displayError(activity, resource, listener, args);
    }

    /**
     * Fragment to display an error and go back to the current activity.
     */
    public static class DisplayMessageDialogFragment extends DialogFragment {
        /**
         * The listener called when the dialog is ended.
         */
        @Nullable
        private MessageDialogListener mListener = null;

        @Override
        public final Dialog onCreateDialog(@Nullable final Bundle savedInstanceState) {
            CharSequence message = getArguments().getCharSequence(PARAM_MESSAGE);
            String title = getArguments().getString(PARAM_TITLE);


            mListener = (MessageDialogListener) getArguments().getSerializable(
                    PARAM_LISTENER);
            getArguments().putSerializable(PARAM_LISTENER, null);

            // Listeners cannot retain functionality when automatically recreated.
            // Therefore, dialogs with listeners must be re-created by the activity on orientation change.
            boolean preventRecreation = false;
            if (savedInstanceState != null) {
                preventRecreation = savedInstanceState.getBoolean("preventRecreation");
            }
            if (preventRecreation) {
                dismiss();
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(@NonNull final DialogInterface dialog, final int id) {
                            if (mListener != null) {
                                mListener.onDialogClick(DisplayMessageDialogFragment.this);
                            }
                            dialog.dismiss();
                        }
                    });
            return builder.create();
        }

        @Override
        public final void onCancel(final DialogInterface dialog) {
            super.onCancel(dialog);
            if (mListener != null) {
                mListener.onDialogCancel(DisplayMessageDialogFragment.this);
            }
        }

        @Override
        public final void onSaveInstanceState(@NonNull final Bundle outState) {
            if (mListener != null) {
                // Typically cannot serialize the listener due to its reference to the activity.
                mListener = null;
                outState.putBoolean("preventRecreation", true);
            }
            super.onSaveInstanceState(outState);
        }

        /**
         * The activity that creates an instance of this dialog listFoldersFragment must implement this interface in
         * order to receive event callbacks. Each method passes the DialogFragment in case the host needs to query it.
         */
        public interface MessageDialogListener extends Serializable {
            /**
             * Callback method for ok click from the dialog.
             *
             * @param dialog the confirmation dialog fragment.
             */
            void onDialogClick(final DialogFragment dialog);

            /**
             * Callback method for cancellation of the dialog.
             *
             * @param dialog the confirmation dialog fragment.
             */
            void onDialogCancel(final DialogFragment dialog);
        }
    }

    public static void showSimpleDialog(Context context, int msgId, DialogInterface.OnClickListener listener) {
        final AlertDialog dialog = new AlertDialog.Builder(context, R.style.Theme_AppCompat_Dialog_Alert)
                .setMessage(msgId)
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, listener)
                .create();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        try {
            dialog.show();
        } catch (Exception e) {
            Activity ownerActivity = dialog.getOwnerActivity();
            L.e(TAG, "showSimpleDialog: can't be shown, activity finished " + (ownerActivity == null ? "null" : ownerActivity.isFinishing()));
            L.exc(e);
            if (listener != null) {
                listener.onClick(null, -1);
            }
        }

    }


    public static void showSingleChoice(Context context, int title, CharSequence[] items, int defaultValue, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.Theme_AppCompat_Dialog_Alert)
                .setSingleChoiceItems(items, defaultValue, listener)
                .setTitle(title);
        final AlertDialog dialog = builder.create();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        dialog.show();
    }


}
